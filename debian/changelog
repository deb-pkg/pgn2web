pgn2web (0.4-3) unstable; urgency=medium

  * debian/control:
    - Bump to Standards-Version 4.4.1. No changes required.
    - Build-depend on libwxgtk3.0-gtk3-dev (Closes: #933451)
    - Upgrade to debhelper compat 12.
    - Change maintainer's e-mail.
  * debian/patches:
    - makefile.patch: Replace hardcoded compiler (Closes: #923106)
      Thanks to Helmut Grohne.

 -- Jose G. López <josgalo@jglopez.name>  Wed, 16 Oct 2019 21:13:27 +0200

pgn2web (0.4-2) unstable; urgency=medium

  * Set myself as Maintainer (Closes: #835308)
  * debian/compat: Upgrade to version 11.
  * debian/control:
    - Bump to Standards-Version 4.1.3. No changes required.
    - Use optional priority.
    - Add Vcs-* fields pointing to Salsa.
  * debian/copyright: Convert to Machine-readable format.
  * debian/patches:
    - compilation.patch: Add to fix compilation errors.
    - makefile.patch: Add to enable hardening flags.
    - install-location.patch: Move install location fix to makefile.patch.
  * debian/rules:
    - Enable hardening flags and use verbose mode.
    - Enable also pie and link with --as-needed flag.
  * debian/watch: Add to track upstream releases.
  * Add and install a desktop file.

 -- Jose G. López <josgalo@gmail.com>  Sun, 01 Apr 2018 09:51:06 +0200

pgn2web (0.4-1.1) unstable; urgency=low

  * Non-maintainer upload.
  * Update to source format "3.0 (quilt)".
  * Update to use wxWidgets 3.0 (new patch wx3.0-compat.patch).
    (Closes: #743334)

 -- Olly Betts <olly@survex.com>  Fri, 06 Jun 2014 09:43:31 +1200

pgn2web (0.4-1) unstable; urgency=low

  * Initial release (Closes: #565158)

 -- Oliver Korff <ok@xynyx.de>  Tue, 25 Aug 2009 12:45:11 +0200
